#!/usr/bin/env python

import os
import sys
import subprocess

dirpath = os.path.dirname(os.path.abspath(__file__))
home = os.getenv('HOME')


def colorize(m='', c='[0m', p=False):
    text = ('\033%s' % c) + (m + '\033[0m' if len(m) > 0 else '')
    if p:
        print (text)
        return
    return text


def cyan(m='', p=True):
    return colorize(m=m, c='[96m', p=p)


def green(m='', p=True):
    return colorize(m=m, c='[92m', p=p)


def red(m='', p=True):
    return colorize(m=m, c='[91m', p=p)


def getPackageManager():
    mngr = {
        'yum': '/etc/redhat-release',
        'pacman': '/etc/arch-release',
        'emerge': '/etc/gentoo-release',
        'zypp': '/etc/SuSE-release',
        'apt-fast': '/usr/bin/apt-fast',
        'apt-get': '/etc/debian_version'
    }
    for manager, location in mngr.items():
        if os.path.isfile(location):
            return manager
    raise Exception('Package manager could not be found')


def gitInstalled():
    try:
        # pipe output to /dev/null for silence
        devnull = open("/dev/null", "w")
        subprocess.Popen("git", stdout=devnull, stderr=devnull)
        devnull.close()
        return True
    except OSError:
        pass
    return False


# start setup
#
if not gitInstalled():
    packages = [
        'git',
        'libyaml-dev',
        'python-pip'
    ]
    pm = getPackageManager()
    commands = []
    if pm == 'apt-get':
        green('Debian based system. Installing apt-fast and git for better downloading...')
        commands.append('wget -O - https://raw.githubusercontent.com/ilikenwf/apt-fast/master/quick-install.sh | bash')
        commands.append('sudo wget https://raw.githubusercontent.com/ilikenwf/apt-fast/master/completions/bash/apt-fast -O /etc/bash_completion.d/apt-fast')
        commands.append('sudo wget https://raw.githubusercontent.com/ilikenwf/apt-fast/master/completions/zsh/_apt-fast -O /usr/share/zsh/functions/Completion/Debian/_apt-fast')
        pm = 'apt-fast'
    else:
        green('Installing git...')

    commands.append('sudo %s install -y %s' % (pm, ' '.join(packages)))

    for command in commands:
        if os.system(command) != 0:
            raise Exception('Failed')

# change to home directory to clone repository
checkoutPath = os.path.join(home, '.mtxr')
if not os.path.isdir(checkoutPath):
    os.makedirs(checkoutPath)
repoUrl = 'https://mtxr@gitlab.com/mtxr/laptop-setup'
repoPath = os.path.join(checkoutPath, repoUrl.split('/').pop())
if os.path.isdir(repoPath):
    red('Already downloaded!!!')
    sys.exit(0)

pydependencies = [
    'pip',
    'ruamel.yaml'
]

p = subprocess.Popen(('pip install --upgrade --user %s' % ' '.join(pydependencies)).split(' '))
if p.wait() != 0:
    raise Exception('Failed')

p = subprocess.Popen(('git clone --recursive %s.git %s' % (repoUrl, repoPath)).split(' '))
if p.wait() != 0:
    raise Exception('Failed')
os.system('sudo ln -snf %s /usr/bin/laptop-manager' % os.path.join(repoPath, 'laptop-manager'))
green('Installed!')
